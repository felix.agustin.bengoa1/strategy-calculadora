﻿using StrategyCalculator.Interfaz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyCalculator.Modelo.StrategyOperaciones
{
    public class StrategyDivision : IOperacionesAritmeticas
    {
        public double Calcular(double numero1, double numero2)
        {
            if (numero2 == 0) {
                throw new DivideByZeroException("el divisor no puede ser cero");
            }
            return numero1 / numero2;
        }
    }
}
