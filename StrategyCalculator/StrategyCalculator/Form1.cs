using StrategyCalculator.Interfaz;
using StrategyCalculator.Modelo;
using StrategyCalculator.Modelo.StrategyOperaciones;

namespace StrategyCalculator
{
    public partial class Form1 : Form
    {
        private Calculadora calculadora;
        public Form1()
        {
            InitializeComponent();
            calculadora = new Calculadora(0,0);
        }

        private void BtnSumar_Click(object sender, EventArgs e)
        {
            ejecutarOperacion(new StrategySuma());
        }

        private void BtnRestar_Click(object sender, EventArgs e)
        {
            ejecutarOperacion(new StrategyResta());
        }

        private void BtnDivision_Click(object sender, EventArgs e)
        {
            ejecutarOperacion(new StrategyDivision());
        }

        private void BtnMultiplicacion_Click(object sender, EventArgs e)
        {
            ejecutarOperacion(new StrategyMultiplicacion());
        }

        private void ejecutarOperacion(IOperacionesAritmeticas operaciones)
        {
            try
            {
                double numero1 = Convert.ToDouble(TxtNumero1.Text);
                double numero2 = Convert.ToDouble(TxtNumero2.Text);

                calculadora = new Calculadora(numero1, numero2);

                double resultado = operaciones.Calcular(calculadora.Numero1, calculadora.Numero2);

                LblResultado.Text = "Resultado: " + resultado.ToString();
            }
            catch (FormatException)
            {
                MessageBox.Show("por favor ingrese numeros validos.");
            }
            catch (DivideByZeroException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("ocurrio un error " + ex.Message);
            }
        }
    }
}
