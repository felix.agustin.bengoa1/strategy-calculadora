﻿using StrategyCalculator.Interfaz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyCalculator.Modelo.StrategyOperaciones
{
    public class StrategyResta : IOperacionesAritmeticas
    {
        public double Calcular(double numero1, double numero2)
        {
            return numero1 - numero2;
        }
    }
}
