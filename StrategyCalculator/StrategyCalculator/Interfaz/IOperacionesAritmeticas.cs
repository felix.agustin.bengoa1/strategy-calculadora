﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyCalculator.Interfaz
{
    public interface IOperacionesAritmeticas
    {
        public double Calcular(double numero1, double numero2);
    }
}
