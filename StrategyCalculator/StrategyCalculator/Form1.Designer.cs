﻿namespace StrategyCalculator
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            LblNumero1 = new Label();
            LblNumero2 = new Label();
            BtnSumar = new Button();
            TxtNumero1 = new TextBox();
            TxtNumero2 = new TextBox();
            BtnRestar = new Button();
            BtnDivision = new Button();
            BtnMultiplicacion = new Button();
            LblResultado = new Label();
            Resultado = new Label();
            SuspendLayout();
            // 
            // LblNumero1
            // 
            LblNumero1.AutoSize = true;
            LblNumero1.Location = new Point(187, 73);
            LblNumero1.Name = "LblNumero1";
            LblNumero1.Size = new Size(78, 20);
            LblNumero1.TabIndex = 0;
            LblNumero1.Text = "Numero 1:";
            // 
            // LblNumero2
            // 
            LblNumero2.AutoSize = true;
            LblNumero2.Location = new Point(187, 115);
            LblNumero2.Name = "LblNumero2";
            LblNumero2.Size = new Size(78, 20);
            LblNumero2.TabIndex = 1;
            LblNumero2.Text = "Numero 2:";
            // 
            // BtnSumar
            // 
            BtnSumar.Location = new Point(269, 159);
            BtnSumar.Name = "BtnSumar";
            BtnSumar.Size = new Size(43, 29);
            BtnSumar.TabIndex = 2;
            BtnSumar.Text = "+";
            BtnSumar.UseVisualStyleBackColor = true;
            BtnSumar.Click += BtnSumar_Click;
            // 
            // TxtNumero1
            // 
            TxtNumero1.Location = new Point(271, 70);
            TxtNumero1.Name = "TxtNumero1";
            TxtNumero1.Size = new Size(92, 27);
            TxtNumero1.TabIndex = 3;
            // 
            // TxtNumero2
            // 
            TxtNumero2.Location = new Point(269, 112);
            TxtNumero2.Name = "TxtNumero2";
            TxtNumero2.Size = new Size(94, 27);
            TxtNumero2.TabIndex = 4;
            // 
            // BtnRestar
            // 
            BtnRestar.Location = new Point(318, 159);
            BtnRestar.Name = "BtnRestar";
            BtnRestar.Size = new Size(45, 29);
            BtnRestar.TabIndex = 5;
            BtnRestar.Text = "-";
            BtnRestar.UseVisualStyleBackColor = true;
            BtnRestar.Click += BtnRestar_Click;
            // 
            // BtnDivision
            // 
            BtnDivision.Location = new Point(269, 194);
            BtnDivision.Name = "BtnDivision";
            BtnDivision.Size = new Size(43, 29);
            BtnDivision.TabIndex = 6;
            BtnDivision.Text = "/";
            BtnDivision.UseVisualStyleBackColor = true;
            BtnDivision.Click += BtnDivision_Click;
            // 
            // BtnMultiplicacion
            // 
            BtnMultiplicacion.Location = new Point(318, 194);
            BtnMultiplicacion.Name = "BtnMultiplicacion";
            BtnMultiplicacion.Size = new Size(45, 29);
            BtnMultiplicacion.TabIndex = 7;
            BtnMultiplicacion.Text = "*";
            BtnMultiplicacion.UseVisualStyleBackColor = true;
            BtnMultiplicacion.Click += BtnMultiplicacion_Click;
            // 
            // LblResultado
            // 
            LblResultado.AutoSize = true;
            LblResultado.Location = new Point(271, 242);
            LblResultado.Name = "LblResultado";
            LblResultado.Size = new Size(93, 20);
            LblResultado.TabIndex = 8;
            LblResultado.Text = "--------------";
            // 
            // Resultado
            // 
            Resultado.AutoSize = true;
            Resultado.Location = new Point(190, 242);
            Resultado.Name = "Resultado";
            Resultado.Size = new Size(78, 20);
            Resultado.TabIndex = 9;
            Resultado.Text = "Resultado:";
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(Resultado);
            Controls.Add(LblResultado);
            Controls.Add(BtnMultiplicacion);
            Controls.Add(BtnDivision);
            Controls.Add(BtnRestar);
            Controls.Add(TxtNumero2);
            Controls.Add(TxtNumero1);
            Controls.Add(BtnSumar);
            Controls.Add(LblNumero2);
            Controls.Add(LblNumero1);
            Name = "Form1";
            Text = "Form1";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label LblNumero1;
        private Label LblNumero2;
        private Button BtnSumar;
        private TextBox TxtNumero1;
        private TextBox TxtNumero2;
        private Button BtnRestar;
        private Button BtnDivision;
        private Button BtnMultiplicacion;
        private Label LblResultado;
        private Label Resultado;
    }
}
